; <?php die(); // prevent direct calls just in case

; userSettings.ini.php
;	Squire 3.0 default configuration file

setupPassword = "Jason1990$"

pageTitle = "Jeff Dawson"
siteTitle = "Welcome"
h1Content = "JEFF DAWSON"

[mainMenu]
home[text] = "Home"
home[href] = ""
faq[text] = "About"
faq[href] = "#about"
contact[text] = "Contact"
contact[href] = "#contact"

[modalForms]
contact['title'] = "@title_contactUsModal"
contact['action'] = "contact"
contact['method'] = "post"

[style]
screen.css = "projection, screen, tv"